@extends('layout')

@section('title', 'Checkout')

@section('extra-css')

@endsection

@section('content')

    <div class="container">
        @if (session()->has('success_message'))
            <div class="spacer"></div>
            <div class="alert alert-success">
                {{ session()->get('success_message') }}
            </div>
        @endif

        @if(count($errors) > 0)
            <div class="spacer"></div>
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <h1 class="checkout-heading stylish-heading">Checkout</h1>
        <div class="checkout-section">
            <div>
                <form action="#">
                    <h2>Detalles de Facturación</h2>

                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" id="email" name="email" value="">
                    </div>
                    <div class="form-group">
                        <label for="name">Nombre</label>
                        <input type="text" class="form-control" id="name" name="name" value="">
                    </div>
                    <div class="form-group">
                        <label for="address">Dirección</label>
                        <input type="text" class="form-control" id="address" name="address" value="">
                    </div>

                    <div class="half-form">
                        <div class="form-group">
                            <label for="city">Ciudad</label>
                            <input type="text" class="form-control" id="city" name="city" value="">
                        </div>
                        <div class="form-group">
                            <label for="province">Provincia</label>
                            <input type="text" class="form-control" id="province" name="province" value="">
                        </div>
                    </div> <!-- end half-form -->

                    <div class="half-form">
                        <div class="form-group">
                            <label for="postalcode">Código Postal</label>
                            <input type="text" class="form-control" id="postalcode" name="postalcode" value="">
                        </div>
                        <div class="form-group">
                            <label for="phone">Teléfono</label>
                            <input type="text" class="form-control" id="phone" name="phone" value="">
                        </div>
                    </div> <!-- end half-form -->

                    <div class="spacer"></div>

                    <h2>Detalles de Pago</h2>

                    <div class="form-group">
                        <label for="name_on_card">Nombre en la tarjeta</label>
                        <input type="text" class="form-control" id="name_on_card" name="name_on_card" value="">
                    </div>
                    <div class="form-group">
                        <label for="address">Dirección</label>
                        <input type="text" class="form-control" id="address" name="address" value="">
                    </div>

                    <div class="form-group">
                        <label for="cc-number">Número de Tarjeta de Crédito</label>
                        <input type="text" class="form-control" id="cc-number" name="cc-number" value="">
                    </div>

                    <div class="half-form">
                        <div class="form-group">
                            <label for="expiry">Fecha de expiración</label>
                            <input type="text" class="form-control" id="expiry" name="expiry" placeholder="MM/DD">
                        </div>
                        <div class="form-group">
                            <label for="cvc">Código CVC</label>
                            <input type="text" class="form-control" id="cvc" name="cvc" value="">
                        </div>
                    </div> <!-- end half-form -->

                    <div class="spacer"></div>

                    <button type="submit" class="button-primary full-width">Completar Orden</button>


                </form>
            </div>


            <div class="checkout-table-container">
                <h2>Su Orden</h2>

                <div class="checkout-table">
                    @foreach( Cart::content() as $item)
                        <div class="checkout-table-row">
                            <div class="checkout-table-row-left">
                                <img src="{{asset('storage/'.$item->model->image) }}" alt="producto"
                                     class="checkout-table-img">
                                <div class="checkout-item-details">
                                    <div class="checkout-table-item">{{ $item->name }}</div>
                                    <div class="checkout-table-description">{{$item->model->details}}</div>
                                    <div class="checkout-table-price">{{ formato_dolar_humano($item->subtotal) }}</div>
                                </div>
                            </div> <!-- end checkout-table -->

                            <div class="checkout-table-row-right">
                                <div class="checkout-table-quantity">{{ $item->qty }}</div>
                            </div>
                        </div> <!-- end checkout-table-row -->
                    @endforeach
                </div> <!-- end checkout-table -->

                <div class="checkout-totals">
                    <div class="checkout-totals-left">
                        Subtotal <br>
                        @if (session()->get('cupon'))
                            Descuento ( {{ session()->get('cupon')['nombre'] }}) {{-- session()->get('cupon') trae un arreglo--}}
                            <form action="{{ route('cupon.destroy') }}" method="POST" style="display: inline">
                                {{csrf_field()}}
                                {{ method_field('DELETE') }}
                                <button type="submit" style="font-size: 14px">Quitar</button>
                            </form>
                            <br>
                        @endif
                        Iva <br>
                        <span class="checkout-totals-total">Total</span>

                    </div>

                    <div class="checkout-totals-right">

                        @if (session()->get('cupon'))
                            {{ formato_dolar_humano( Cart::subtotal(2, ".", "") - session()->get('cupon')['descuento'] ) }} <br> {{-- subtotal (menos) descuento--}}
                            -{{ formato_dolar_humano( session()->get('cupon')['descuento'] ) }} <br>
                        @else
                            {{ formato_dolar_humano( Cart::subtotal(2, ".", "") ) }} <br> {{-- subtotal sin descuento--}}
                        @endif
                        {{ formato_dolar_humano( Cart::tax(2, ".", "") ) }} <br>
                        <span class="checkout-totals-total">{{ formato_dolar_humano(Cart::total(2, ".", "")) }}</span>

                    </div>
                </div> <!-- end checkout-totals -->

                @if( !session()->get('cupon'))
                <a href="#" class="have-code">Have a Code?</a>

                <div class="have-code-container">
                    <form method="POST" action=" {{ route('cupon.store') }} ">
                        {{ csrf_field() }}
                        <input name="nombre_cupon" type="text">
                        <button type="submit" class="button button-plain">Apply</button>
                    </form>
                </div> <!-- end have-code-container -->
                @endif
            </div>

        </div> <!-- end checkout-section -->
    </div>

@endsection
