<?php

use App\Cupon;
use Illuminate\Database\Seeder;

class CuponesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cupones')->insert([
            'codigo' => 'quimpec2020',
            'tipo' => 'valor_fijo',
            'valor' => 3000,
        ]);

        DB::table('cupones')->insert([
            'codigo' => 'quimpec5',
            'tipo' => 'porcentaje_descuento',
            'porcentaje_descuento' => 50,
        ]);
    }
}
