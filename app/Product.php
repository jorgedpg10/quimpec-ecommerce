<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Product extends Model
{

    public function scopeMigthAlsolike($query){
        return $query->inRandomOrder()->take(4);
    }


    function asDollars() {
        return '$' . number_format($this->price / 100, 2);
    }

    function numericCart($precio){
        return number_format($precio / 100, 2);
    }

    public function categoria() {
        return $this->belongsToMany('App\Categoria', 'categoria_producto');
    }
}
