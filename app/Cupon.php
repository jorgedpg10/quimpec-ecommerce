<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cupon extends Model
{
    protected $table = 'cupones';

    public static function findByCode($code)
    {
        return self::where('code', $code)->first();
    }

    public function descuento($total){
        if($this->tipo == 'valor_fijo'){
            return $this->valor;
        }elseif($this->tipo == 'porcentaje_descuento'){

            $descuento =  cast_de_string_a_float($total) * $this->porcentaje_descuento  /100;
            $descuento = acota_en_dos_decimales($descuento);
            return $descuento;
        }else{
            return 0;
        }
    }
}
